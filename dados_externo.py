"""

###################################################
#                                                 #
#        Brasília - DF. Novembro de 2023.         #
#                                                 #
#     TCC e PIC - REDE MESH NA AGRICULTURA.       #
#                                                 #
#        Autor: Raphael Vilela de Paiva.          #
#                                                 #
###################################################

"""

import csv

with open("dados/1528197600.txt", 'r') as f:
    lines = f.readlines()

with open('dados_externo.txt', 'w') as f:
    for i in range(0, len(lines), 2):
        f.write(lines[i].strip() + ' ' + lines[i + 1])

with open('dados_externo.txt', 'r') as f:
    lines = f.readlines()

data = []
for line in lines:
    parts = line.split(' ')
    date = parts[1]
    time = parts[2]
    temp_solo = float(parts[7].split('*')[0])
    temp_ar = float(parts[11].split('*')[0])
    hum_ar = (float(parts[15].split('%')[0])) / 100
    hum_solo = (float(parts[20].split('%')[0])) / 100
    t_solo = "{:.4f}".format(temp_solo).replace(".", ",")
    t_ar = "{:.4f}".format(temp_ar).replace(".", ",")
    h_ar = "{:.4f}".format(hum_ar).replace(".", ",")
    h_solo = "{:.4f}".format(hum_solo).replace(".", ",")
    data.append((date, time, t_solo, t_ar, h_ar, h_solo))

with open('all_data_externo.csv', 'w', newline='') as f:
    writer = csv.writer(f, delimiter=';')
    writer.writerow(['Tempo', '', 'Temperatura do', 'Temperatura do', 'Umiadade do', 'Umiadade do'])
    writer.writerow(['Data', 'Hora', 'Solo (ºC)', 'Ar (ºC)', 'Solo (%)', 'Ar (%)'])
    for row in data:
        writer.writerow([row[0], row[1], row[2], row[3], row[5], row[4]])



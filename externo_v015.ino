/*

###################################################
#                                                 #
#        Brasília - DF. Novembro de 2023.         #
#                                                 #
#     TCC e PIC - REDE MESH NA AGRICULTURA.       #
#                                                 #
#        Autor: Raphael Vilela de Paiva.          #
#                                                 #
###################################################

*/

// Inclui as bibliotecas necessárias
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_AHTX0.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <painlessMesh.h>

// Define os pinos e constantes usados
#define SOIL_MOISTURE_PIN A0
#define DS18B20_PIN D5
#define   MESH_SSID       "agro_mesh"
#define   MESH_PASSWORD   "agro_mesh_00"
#define   MESH_PORT       5555

// Inicializa os objetos para os sensores e a rede mesh
Adafruit_AHTX0 aht;
OneWire oneWire(DS18B20_PIN);
DallasTemperature sensors(&oneWire);
painlessMesh  mesh;
Scheduler scheduler;

// Protótipo da função sendMessage
void sendMessage() ;

// Cria uma tarefa para chamar a função sendMessage a cada 30 segundos
Task taskSendMessage( TASK_SECOND * 30, TASK_FOREVER, &sendMessage );

// Função para enviar uma mensagem com os dados dos sensores
void sendMessage() {
  // Solicita a temperatura do sensor DS18B20
  sensors.requestTemperatures();
  float tempDS18B20 = sensors.getTempCByIndex(0);

  // Obtém a umidade e a temperatura do sensor AHT10
  sensors_event_t humidity, temp;
  aht.getEvent(&humidity, &temp);
  float humAHT10 = humidity.relative_humidity;
  float tempAHT10 = temp.temperature;

  // Lê a umidade do solo
  int soilMoistureRaw = analogRead(SOIL_MOISTURE_PIN);
  float soilMoisturePercent = map(soilMoistureRaw, 0, 1023, 0, 100);

  // Imprime os dados no Monitor Serial
  Serial.print("Temperatura DS18B20: ");
  Serial.print(tempDS18B20);
  Serial.print(" *C, Temperatura AHT10: ");
  Serial.print(tempAHT10);
  Serial.print(" *C, Umidade AHT10: ");
  Serial.print(humAHT10);
  Serial.print(" %, Umidade do Solo: ");
  Serial.print(soilMoisturePercent);
  Serial.println(" %");

  // Cria uma mensagem com os dados dos sensores
  String msg = "externo - Temperatura DS18B20: ";
  msg += tempDS18B20;
  msg += " *C, Temperatura AHT10: ";
  msg += tempAHT10;
  msg += " *C, Umidade AHT10: ";
  msg += humAHT10;
  msg += " %, Umidade do Solo: ";
  msg += soilMoisturePercent;
  msg += " %";

  // Imprime a mensagem enviada no Monitor Serial
  Serial.println("Mensagem enviada: " + msg);

  // Envia a mensagem para a rede mesh
  mesh.sendBroadcast(msg);
}

// Função setup, chamada uma vez quando o programa começa
void setup() {
  // Inicializa o Monitor Serial
  Serial.begin(9600);

  // Inicializa o sensor AHT10
  if (!aht.begin()) {
    Serial.println("Falha na inicialização do sensor AHT10.");
    return;
  }

  // Inicializa o sensor DS18B20
  sensors.begin();

  // Configura e inicializa a rede mesh
  mesh.setDebugMsgTypes(ERROR | STARTUP);
  mesh.init(MESH_SSID, MESH_PASSWORD, &scheduler, MESH_PORT);

  // Adiciona a tarefa para enviar a mensagem à programação
  scheduler.addTask( taskSendMessage );
  taskSendMessage.enable();
}

// Função loop, chamada repetidamente em um loop
void loop() {
  // Mantém a rede mesh funcionando
  mesh.update();
}

"""

###################################################
#                                                 #
#        Brasília - DF. Novembro de 2023.         #
#                                                 #
#     TCC e PIC - REDE MESH NA AGRICULTURA.       #
#                                                 #
#        Autor: Raphael Vilela de Paiva.          #
#                                                 #
###################################################

"""

import csv

with open('dados/home.txt', 'r') as f:
    lines = f.readlines()

with open('dados_home.txt', 'w') as f:
    for i in range(0, len(lines), 2):
        f.write(lines[i].strip() + ' ' + lines[i+1])

with open('dados_home.txt', 'r') as f:
    lines = f.readlines()

data = []
for line in lines:
    parts = line.split(' ')
    date = parts[1]
    time = parts[2]
    temp = float(parts[7].split('*')[0])
    humd = (float(parts[11].split('%')[0]))/100
    press = float(parts[15].split('h')[0])
    t_ar = "{:.4f}".format(temp).replace(".",",")
    h_ar = "{:.4f}".format(humd).replace(".",",")
    p_ar = "{:.4f}".format(press).replace(".",",")
    data.append((date, time, t_ar, h_ar, p_ar))

with open('all_data_home.csv', 'w', newline='') as f:
    writer = csv.writer(f, delimiter=';')
    writer.writerow(['Tempo', '', 'Sensor AHT10', '', '', '', '', ''])
    writer.writerow(['Data', 'Hora', 'Temperatura (ºC)', 'Umidade (%)', 'Pressão (hPa)'])
    for row in data:
        writer.writerow([row[0], row[1], row[2], row[3], row[4]])



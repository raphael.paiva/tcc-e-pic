/*

###################################################
#                                                 #
#        Brasília - DF. Novembro de 2023.         #
#                                                 #
#     TCC e PIC - REDE MESH NA AGRICULTURA.       #
#                                                 #
#        Autor: Raphael Vilela de Paiva.          #
#                                                 #
###################################################

*/

// Incluindo as bibliotecas necessárias
#include <painlessMesh.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <SD.h>
#include <RTClib.h>

// Definindo constantes para a rede mesh e o pino do cartão SD
#define   MESH_SSID       "agro_mesh"
#define   MESH_PASSWORD   "agro_mesh_00"
#define   MESH_PORT       5555
#define   SD_CS_PIN       D8

// Criando instâncias dos objetos necessários
Adafruit_BME280 bme;
painlessMesh  mesh;
Scheduler scheduler;
RTC_DS1307 rtc;

// Variáveis para controle de tempo
unsigned long previousMillis = 0; 
const long interval = 30000;

// Função chamada quando uma mensagem é recebida na rede mesh
void receivedCallback(uint32_t from, String &msg) {
  // Imprime a mensagem recebida
  Serial.println("Mensagem recebida: " + msg);

  // Cria um nome de arquivo com base no ID do nó que enviou a mensagem
  String fileName = "/" + String(from) + ".txt";

  // Abre o arquivo para escrita
  File dataFile = SD.open(fileName.c_str(), FILE_WRITE);

  // Se não conseguiu abrir o arquivo, imprime uma mensagem de erro e retorna
  if (!dataFile) {
    Serial.println("Falha ao abrir o arquivo de dados para escrita");
    return;
  }

  // Obtém a data e hora atual
  DateTime now = rtc.now();
  // Escreve a data, a hora e a mensagem no arquivo
  dataFile.println("Tempo: " + String(now.day()) + "/" + String(now.month()) + "/" + String(now.year()) + " " + String(now.hour()) + ":" + String(now.minute()) + ":" + String(now.second()));
  dataFile.println(msg);
  // Fecha o arquivo
  dataFile.close();
}

// Função de configuração, executada uma vez quando o ESP8266 é iniciado
void setup() {
  // Inicia a comunicação serial
  Serial.begin(9600);

  // Inicia o cartão SD
  if (!SD.begin(SD_CS_PIN)) {
    // Se falhou, imprime uma mensagem de erro e retorna
    Serial.println("Falha ao montar o sistema de arquivos");
    return;
  }

  // Inicia o sensor BME280
  if (!bme.begin(0x76)) {
    // Se falhou, imprime uma mensagem de erro e retorna
    Serial.println("Falha na inicialização do sensor BME280.");
    return;
  }

  // Inicia o RTC
  if (!rtc.begin()) {
    // Se falhou, imprime uma mensagem de erro e retorna
    Serial.println("Não foi possível encontrar um módulo RTC válido.");
    return;
  }

  // Verifica se o RTC está funcionando
  if (!rtc.isrunning()) {
    // Se não está funcionando, imprime uma mensagem de erro e ajusta a data e a hora
    Serial.println("RTC não está funcionando!");
    rtc.adjust(DateTime(2023, 11, 10, 7, 42, 30));
  }

  // Configura a rede mesh
  mesh.setDebugMsgTypes(ERROR | STARTUP);
  mesh.init(MESH_SSID, MESH_PASSWORD, &scheduler, MESH_PORT);

  // Define a função que será chamada quando uma mensagem for recebida
  mesh.onReceive(&receivedCallback);
}

// Função loop, executada repetidamente enquanto o ESP8266 estiver ligado
void loop() {
  // Atualiza a rede mesh
  mesh.update();

  // Verifica se já passou o intervalo de tempo definido
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    // Se passou, atualiza a variável previousMillis
    previousMillis = currentMillis;

    // Lê os valores do sensor BME280
    float tempBME280 = bme.readTemperature();
    float humBME280 = bme.readHumidity();
    float presBME280 = bme.readPressure() / 100.0F;

    // Obtém a data e hora atual
    DateTime now = rtc.now();
    // Imprime a data e a hora
    Serial.print("Tempo: ");
    Serial.print(now.day(), DEC);
    Serial.print('/');
    Serial.print(now.month(), DEC);
    Serial.print('/');
    Serial.print(now.year(), DEC);
    Serial.print(" ");
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.println();

    // Imprime os valores lidos do sensor
    Serial.print("Temperatura BME280: ");
    Serial.print(tempBME280);
    Serial.print(" *C, Umidade BME280: ");
    Serial.print(humBME280);
    Serial.print(" %, Pressão BME280: ");
    Serial.print(presBME280);
    Serial.println(" hPa");

    // Cria uma string com os valores lidos do sensor
    String msg = "home - Temperatura BME280: ";
    msg += tempBME280;
    msg += " *C, Umidade BME280: ";
    msg += humBME280;
    msg += " %, Pressão BME280: ";
    msg += presBME280;
    msg += " hPa";

    // Cria um nome de arquivo
    String fileName = "/home.txt";
    // Abre o arquivo para escrita
    File dataFile = SD.open(fileName.c_str(), FILE_WRITE);

    // Se não conseguiu abrir o arquivo, imprime uma mensagem de erro e retorna
    if (!dataFile) {
      Serial.println("Falha ao abrir o arquivo de dados para escrita");
      return;
    }

    // Escreve a data, a hora e a mensagem no arquivo
    dataFile.print("Tempo: ");
    dataFile.print(now.day(), DEC);
    dataFile.print('/');
    dataFile.print(now.month(), DEC);
    dataFile.print('/');
    dataFile.print(now.year(), DEC);
    dataFile.print(" ");
    dataFile.print(now.hour(), DEC);
    dataFile.print(':');
    dataFile.print(now.minute(), DEC);
    dataFile.print(':');
    dataFile.print(now.second(), DEC);
    dataFile.println();
    dataFile.println(msg);
    // Fecha o arquivo
    dataFile.close();
  }
}
